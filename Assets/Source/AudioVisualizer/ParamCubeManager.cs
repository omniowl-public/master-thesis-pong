﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Thesis.AudioVisualizer
{
    public class ParamCubeManager : MonoBehaviour
    {
        public GameObject paramCubePrefab;
        public float startScale;
        public float maxScale;
        public float radius = 1.0f;

        public void Start()
        {
            for (int i = 0; i < 8; i++)
            {
                GameObject instance = Instantiate(paramCubePrefab);
                instance.transform.parent = transform;
                float angle = i * Mathf.PI * 2f / 8;
                instance.transform.localPosition = new Vector3(Mathf.Cos(angle) * radius, -1f, Mathf.Sin(angle) * radius);
                Vector3 newRot = transform.position - instance.transform.position;
                instance.transform.localRotation = Quaternion.FromToRotation(instance.transform.up, new Vector3(0, newRot.y, 0));
                ParamCube cubeComp = instance.GetComponent<ParamCube>();
                cubeComp._startScale = startScale;
                cubeComp._maxScale = maxScale;
                cubeComp._band = i;
                cubeComp._useBuffer = true;
            }
        }
    }
}
