﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Thesis.AudioVisualizer
{
    public class ParamCube : MonoBehaviour
    {
        [Range(0.05f, 1.0f)]
        public float colorMultiplier;
        public bool _useBuffer;
        public int _band;
        public float _startScale;
        public float _maxScale;

        private Material _material;

        public void Start()
        {
            _material = GetComponent<MeshRenderer>().materials[0];
        }

        // Update is called once per frame
        void Update()
        {
            float x = transform.localScale.x;
            float z = transform.localScale.z;
            float y = 1.0f;

            Color emissionColor = new Color();

            if (_useBuffer == true)
            {
                y = (AudioVisualizer._audioBandBuffer[_band] * _maxScale) + _startScale;
                float audioBufferValue = AudioVisualizer._audioBandBuffer[_band] * colorMultiplier;
                emissionColor = new Color(audioBufferValue, audioBufferValue, audioBufferValue);
            }
            else
            {
                y = (AudioVisualizer._audioBand[_band] * _maxScale) + _startScale;
                float audioBandValue = AudioVisualizer._audioBand[_band] * colorMultiplier;
                emissionColor = new Color(audioBandValue, audioBandValue, audioBandValue);
            }

            if (y.Equals(float.NaN))
            {
                y = 1.0f;
            }

            transform.localScale = new Vector3(x, y, z);
            _material.SetColor("_EmissionColor", emissionColor);
        }
    }
}