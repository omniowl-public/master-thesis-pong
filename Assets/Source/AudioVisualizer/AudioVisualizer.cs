﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Thesis.AudioVisualizer
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioVisualizer : MonoBehaviour
    {
        public float BufferDecreaseFactor = 1.2f;
        public float BufferDecreaseValue = 0.005f;
        public FFTWindow FFTWindowType = FFTWindow.Blackman;

        public static float[] _audioBand = new float[8];
        public static float[] _audioBandBuffer = new float[8];

        private AudioSource _aSource;
        private float[] _samples = new float[512];
        private float[] _freqBand = new float[8];
        private float[] _bandBuffer = new float[8];
        private float[] _bufferDecrease = new float[8];
        private float[] _freqBandHighest = new float[8];

        // Start is called before the first frame update
        void Start()
        {
            _aSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            GetSpectrumFromAudioSource();
            MakeFrequencyBands();
            BandBuffer();
            CreateAudioBands();
        }

        public float GetSample(int index)
        {
            return _samples[index];
        }

        private void CreateAudioBands()
        {
            for (int i = 0; i < _freqBandHighest.Length; i++)
            {
                if (_freqBand[i] > _freqBandHighest[i])
                {
                    _freqBandHighest[i] = _freqBand[i];
                }
                _audioBand[i] = (_freqBand[i] / _freqBandHighest[i]);
                _audioBandBuffer[i] = (_bandBuffer[i] / _freqBandHighest[i]);
            }
        }

        private void GetSpectrumFromAudioSource()
        {
            _aSource.GetSpectrumData(_samples, 0, FFTWindowType);
        }

        private void BandBuffer()
        {
            for (int g = 0; g < _freqBand.Length; g++)
            {
                if (_freqBand[g] > _bandBuffer[g])
                {
                    _bandBuffer[g] = _freqBand[g];
                    _bufferDecrease[g] = BufferDecreaseValue;
                }

                if (_freqBand[g] < _bandBuffer[g])
                {
                    _bandBuffer[g] -= _bufferDecrease[g];
                    _bufferDecrease[g] *= BufferDecreaseFactor;
                }
            }
        }

        private void MakeFrequencyBands()
        {
            int count = 0;
            for (int i = 0; i < _freqBand.Length; i++)
            {
                float average = 0;
                int sampleCount = (int)Mathf.Pow(2, i) * 2;

                // to include the whole spectrum
                if (i == 7)
                {
                    sampleCount += 2;
                }

                for (int j = 0; j < sampleCount; j++)
                {
                    average += _samples[count] * (count + 1);
                    count++;
                }

                average /= count;
                _freqBand[i] = average * 10;
            }
        }
    }
}