﻿using GoogleARCore;
using GoogleARCore.Examples.CloudAnchors;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.ARPong.Cloud
{
    public abstract class ARPongAbstractClient : MonoBehaviour
    {
        public enum ApplicationMode
        {
            Ready,
            Hosting,
            Resolving
        }

        public string searchMessage = "";
        public ARScreenUIManager m_ScreenUIManager;
        public Camera FirstPersonCamera;
        public string AnchorPrefabName;

        [SerializeField]
        internal string SharedAnchorId;
        internal List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();

        internal ApplicationMode m_CurrentMode;
        internal bool m_IsOriginPlaced = false;
        internal bool m_AnchorAlreadyInstantiated = false;
        internal ARCoreWorldOriginHelper m_ARCoreWorldOriginHelper;
        internal Component m_LastPlacedAnchor;
        internal ARPongSessionHandler m_SessionHandler;

        public virtual void Start() { }
        public virtual void Update()
        {
            Session.GetTrackables(m_AllPlanes);
            bool showSearchMessage = true;
            for (int i = 0; i < m_AllPlanes.Count; i++)
            {
                if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
                {
                    showSearchMessage = false;
                    break;
                }
            }

            if (showSearchMessage == true)
            {
                m_ScreenUIManager.SetSnackbarText(searchMessage);
            }

            if (m_CurrentMode != ApplicationMode.Hosting && m_CurrentMode != ApplicationMode.Resolving)
            {
                return;
            }

            // If the origin anchor has not been placed yet, then update in resolving mode is complete.
            if (m_CurrentMode == ApplicationMode.Resolving && !m_IsOriginPlaced)
            {
                return;
            }
        }

        /// <summary>
        /// Sets the apparent world origin so that the Origin of Unity's World Coordinate System coincides with the
        /// Anchor. This function needs to be called once the Cloud Anchor is either hosted or resolved.
        /// </summary>
        /// <param name="anchorTransform">Transform of the Cloud Anchor.</param>
        internal void SetWorldOrigin(Transform anchorTransform)
        {
            if (m_IsOriginPlaced)
            {
                return;
            }

            m_IsOriginPlaced = true;
            m_ARCoreWorldOriginHelper.SetWorldOrigin(anchorTransform);
        }
    }
}
