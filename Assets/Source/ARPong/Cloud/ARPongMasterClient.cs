﻿using ExitGames.Client.Photon;
using GoogleARCore;
using GoogleARCore.Examples.CloudAnchors;
using Photon.Pun;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Thesis.ARPong.Cloud
{
    public class ARPongMasterClient : ARPongAbstractClient
    {
        public event EventHandler<string> OnAnchorHosted;

        public bool IsTesting = true;
        private ARPongGameManager m_GameManager;

        public override void Start()
        {
            m_CurrentMode = ApplicationMode.Hosting;
        }

        public override void Update()
        {
            base.Update();

            if (m_LastPlacedAnchor == null)
            {
                if (IsTesting == false)
                {
                    Touch touch;
                    if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
                    {
                        return;
                    }
                    TrackableHit hit;
                    if (m_ARCoreWorldOriginHelper.Raycast(
                        touch.position.x, touch.position.y,
                        TrackableHitFlags.PlaneWithinPolygon, out hit))
                    {
                        m_LastPlacedAnchor = hit.Trackable.CreateAnchor(hit.Pose);
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        TrackableHit hit;
                        Vector2 mousePos = Input.mousePosition;
                        if (m_ARCoreWorldOriginHelper.Raycast(
                            mousePos.x, mousePos.y,
                            TrackableHitFlags.PlaneWithinPolygon, out hit))
                        {
                            m_LastPlacedAnchor = hit.Trackable.CreateAnchor(hit.Pose);
                        }
                    }
                }
            }

            if (m_LastPlacedAnchor != null)
            {
                if (!m_IsOriginPlaced)
                {
                    SetWorldOrigin(m_LastPlacedAnchor.transform);
                    _InstantiateAnchor();
                    OnAnchorInstantiated();
                }
            }
        }

        private void _InstantiateAnchor()
        {
            // Instantiate Anchor model at the hit pose.
            GameObject anchorObject = PhotonNetwork.Instantiate(AnchorPrefabName, Vector3.zero, Quaternion.identity, 0);
            anchorObject.transform.parent = m_LastPlacedAnchor.transform;
            anchorObject.transform.localPosition = new Vector3(0, 0.5f, 0);
            anchorObject.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            m_GameManager = anchorObject.GetComponent<ARPongGameManager>();
            m_GameManager.StartGame();

            string result = m_SessionHandler.HostLastPlacedAnchor(m_LastPlacedAnchor);
            if (!string.IsNullOrEmpty(result))
            {
                SharedAnchorId = result;
                OnAnchorHosted?.Invoke(this, SharedAnchorId);
            }
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was instantiated and the host request was made.
        /// </summary>
        private void OnAnchorInstantiated()
        {
            if (m_AnchorAlreadyInstantiated)
            {
                return;
            }

            m_AnchorAlreadyInstantiated = true;
            m_ScreenUIManager.SetSnackbarText("Hosting Cloud Anchor...");
        }
    }
}