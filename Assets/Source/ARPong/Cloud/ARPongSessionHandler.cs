﻿using GoogleARCore;
using GoogleARCore.CrossPlatform;
using GoogleARCore.Examples.Common;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.ARPong.Cloud
{
    public class ARPongSessionHandler : MonoBehaviour
    {
        internal bool m_ShouldResolve = false;
        internal string m_CloudAnchorId;
        internal ARScreenUIManager m_ScreenUIManager;
        internal ARPongUserClient client;

        public void Update()
        {
            if (m_ShouldResolve)
            {
                _ResolveAnchorFromId(m_CloudAnchorId);
            }
        }

        public string HostLastPlacedAnchor(Component lastPlacedAnchor)
        {
            var anchor = (Anchor)lastPlacedAnchor;
            string cloudId = "";
            XPSession.CreateCloudAnchor(anchor).ThenAction(result =>
            {
                if (result.Response != CloudServiceResponse.Success)
                {
                    Debug.Log(string.Format("Failed to host Cloud Anchor: {0}", result.Response));
                    m_ScreenUIManager.SetSnackbarText($"Failed to host Cloud Anchor: {result.Response}");
                    return;
                }

                Debug.Log(string.Format("Cloud Anchor {0} was created and saved.", result.Anchor.CloudId));

                cloudId = result.Anchor.CloudId;
                m_ScreenUIManager.SetSnackbarText($"Cloud Anchor {result.Anchor.CloudId} was created and saved.");
            });

            return cloudId;
        }

        public void _ResolveAnchorFromId(string cloudAnchorId)
        {
            m_ScreenUIManager.SetSnackbarText("Attempting to Resolve Anchor...");

            // If device is not tracking, let's wait to try to resolve the anchor.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

            m_ShouldResolve = false;

            XPSession.ResolveCloudAnchor(cloudAnchorId).ThenAction((System.Action<CloudAnchorResult>)(result =>
            {
                if (result.Response != CloudServiceResponse.Success)
                {
                    Debug.LogError(string.Format("Client could not resolve Cloud Anchor {0}: {1}",
                                                 cloudAnchorId, result.Response));
                    m_ScreenUIManager.SetSnackbarText(string.Format("Client could not resolve Cloud Anchor {0}: {1}",
                                                 cloudAnchorId, result.Response));
                    m_ShouldResolve = true;
                    return;
                }

                Debug.Log(string.Format("Client successfully resolved Cloud Anchor {0}.",
                                        cloudAnchorId));
                m_ScreenUIManager.SetSnackbarText(string.Format("Client successfully resolved Cloud Anchor {0}.",
                                        cloudAnchorId));
                client.SetWorldOrigin(result.Anchor.transform);
            }));
        }
    }
}
