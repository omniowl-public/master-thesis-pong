﻿using GoogleARCore;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Thesis.ARPong.Cloud
{
    public class ARPongUserClient : ARPongAbstractClient
    {
        public override void Start()
        {
            m_CurrentMode = ApplicationMode.Resolving;
        }

        public override void Update()
        {
            base.Update();
            if (m_SessionHandler.m_ShouldResolve == false)
            {
                foreach (DetectedPlane plane in m_AllPlanes)
                {
                    if (plane.TrackingState == TrackingState.Tracking && (plane.ExtentX * plane.ExtentZ) > 2)
                    {
                        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey("SharedAnchor") == true)
                        {
                            SharedAnchorId = (string)PhotonNetwork.CurrentRoom.CustomProperties["SharedAnchor"];
                            m_SessionHandler.m_CloudAnchorId = SharedAnchorId;
                            m_SessionHandler.m_ShouldResolve = true;
                        }
                        break;
                    }
                }
            }
        }
    }
}