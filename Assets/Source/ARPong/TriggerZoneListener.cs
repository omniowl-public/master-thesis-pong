﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Thesis.ARPong
{
    public class TriggerZoneListener : MonoBehaviour
    {
        [Tooltip("Zero Based ID")]
        [Range(0, 1)]
        public int PlayerId;
        public event EventHandler OnTriggerEntered;

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Ball")
            {
                if (OnTriggerEntered != null)
                {
                    OnTriggerEntered(PlayerId, null);
                }
            }
        }
    }
}