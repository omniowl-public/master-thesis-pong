﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using Thesis.ARPong.Cloud;
using UnityEngine;

namespace Thesis.ARPong
{
    [RequireComponent(typeof(ARPongUIController))]
    public class ARPongGameManager : MonoBehaviour
    {
        public event EventHandler<int> OnPlayerScored;

        public GameObject BallPrefab;
        public TriggerZoneListener player1Zone;
        public TriggerZoneListener player2Zone;
        private ARPongUIController UIController;
        private GameObject BallObject;

        void Start()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                UIController = GetComponent<ARPongUIController>();
                player1Zone.OnTriggerEntered += Listener_OnTriggerEntered;
                player2Zone.OnTriggerEntered += Listener_OnTriggerEntered;
            }
        }

        public void StartGame()
        {
            BallObject = PhotonNetwork.Instantiate("Ball", Vector3.zero, Quaternion.identity, 0);
            BallObject.transform.parent = transform;
            BallObject.transform.localPosition = new Vector3(0, 0.75f, 1);
            BallObject.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
            BallObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0.5f, 0.5f);
        }

        public void ResetBall()
        {
            BallObject.transform.localPosition = new Vector3(0, 0.75f, 1);
        }

        private void Listener_OnTriggerEntered(object sender, System.EventArgs e)
        {
            int PlayerId = (int)sender;
            Vector3 newVelocity = Vector3.zero;
            if (PlayerId == 1)
            {
                UIController.AddOneToScore(0);
                newVelocity.y = 0.5f;
                newVelocity.z = -0.5f;
            }
            else
            {
                UIController.AddOneToScore(1);
                newVelocity.y = 0.5f;
                newVelocity.z = 0.5f;
            }

            if (OnPlayerScored != null)
            {
                OnPlayerScored(this, PlayerId);
            }

            BallObject.transform.localPosition = new Vector3(0, 0.75f, 1);
            BallObject.GetComponent<Rigidbody>().velocity = newVelocity;
        }
    }
}