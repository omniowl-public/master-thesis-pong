﻿using ExitGames.Client.Photon;
using GoogleARCore;
using GoogleARCore.Examples.CloudAnchors;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using Thesis.ARPong.Cloud;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.ARPong
{
    public class ARPongLauncher : MonoBehaviourPunCallbacks
    {
        public event EventHandler<string> OnAnchorHosted;
        public event EventHandler<int> OnPlayerScored;

        public ARScreenUIManager m_ScreenUIManager;
        public float RequiredScannedSpaceSize = 5.0f;

        [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        [SerializeField]
        public byte maxPlayersPerRoom = 8;
        [SerializeField]
        public Button CreateRoomButton;
        [SerializeField]
        public Button JoinRoomButton;
        [SerializeField]
        public InputField NameInputField;
        [SerializeField]
        public Animator DialogAnimator;
        [SerializeField]
        public InputField DialogInputField;
        [SerializeField]
        public Button DialogSubmitButton;
        [SerializeField]
        public string PlayerPrefabName;
        [SerializeField]
        public string AnchorPrefabName;
        [SerializeField]
        public AnimationEventListener dialogListener;

        // Private Fields

        const string PlayerPrefNameKey = "PlayerName";
        private TypedLobby Lobby;
        private string GameVersion = "1";
        private bool IsDialogShown = false;
        private bool IsDialogCreateRoom = false;
        private GameObject PlayerPrefab;
        private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();

        private float HighestPlaneSize = 0.0f;

        internal bool m_IsQuitting = false;

        // MonoBehaviour CallBacks

        private void Awake()
        {
            PlayerPrefab = GameObject.Find(PlayerPrefabName);
            PhotonNetwork.AutomaticallySyncScene = true;
            Connect();
        }

        private void Start()
        {
            Lobby = new TypedLobby()
            {
                Name = "ARPongLobby",
                Type = LobbyType.Default
            };

            if (PlayerPrefs.HasKey(PlayerPrefNameKey))
            {
                NameInputField.text = PlayerPrefs.GetString(PlayerPrefNameKey);
                PhotonNetwork.NickName = NameInputField.text;
            }
            else
            {
                NameInputField.text = $"Player{UnityEngine.Random.Range(0, 9000)}";
                PlayerPrefs.SetString("PlayerName", NameInputField.text);
                PhotonNetwork.NickName = NameInputField.text;
            }
            DialogInputField.onValueChanged.AddListener(ListenForDialogInputChange);
            NameInputField.onValueChanged.AddListener(ListenForNameInputChange);
            dialogListener.OnEvent += DialogListener_OnEvent;

            m_ScreenUIManager.ToggleLobbyUI(false);
            m_ScreenUIManager.SetTopbarLabelText("Area Size:");
            m_ScreenUIManager.SetTopbarCountText("0");
            StartCoroutine(CheckPlaneSize());
        }

        private void InitializePhotonUI()
        {
            m_ScreenUIManager.ToggleLobbyUI(true);
        }

        private void Update()
        {
            _UpdateApplicationLifecycle();
            if (HighestPlaneSize < RequiredScannedSpaceSize)
            {
                Session.GetTrackables(m_AllPlanes);
                foreach (DetectedPlane plane in m_AllPlanes)
                {
                    if (plane.TrackingState == TrackingState.Tracking)
                    {
                        float area = plane.ExtentX * plane.ExtentZ;
                        HighestPlaneSize = (area > HighestPlaneSize ? area : HighestPlaneSize);
                        m_ScreenUIManager.SetTopbarCountText(HighestPlaneSize.ToString("#.##"));
                        if (HighestPlaneSize >= RequiredScannedSpaceSize)
                        {
                            break;
                        }
                    }
                }
            }
        }

        // Private Methods

        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        private void ListenForDialogInputChange(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                if (DialogSubmitButton.interactable == true)
                {
                    DialogSubmitButton.interactable = false;
                }
            }
            else
            {
                if (DialogSubmitButton.interactable != true)
                {
                    DialogSubmitButton.interactable = true;
                }
            }
        }

        private void ListenForNameInputChange(string text)
        {
            string newPlayerName = "";
            if (string.IsNullOrEmpty(text))
            {
                newPlayerName = $"Player{UnityEngine.Random.Range(0, 9000)}";
            }
            else
            {
                newPlayerName = NameInputField.text;
            }
            PlayerPrefs.SetString(PlayerPrefNameKey, newPlayerName);
            PhotonNetwork.NickName = newPlayerName;
        }

        private void DialogListener_OnEvent(object sender, EventArgs e)
        {
            DialogInputField.DeactivateInputField();
            DialogInputField.text = "";
            CreateRoomButton.interactable = true;
            JoinRoomButton.interactable = true;
        }

        private void Connect()
        {
            if (!PhotonNetwork.IsConnected)
            {
                m_ScreenUIManager.SetSnackbarText("Waiting for Connection...");
                PhotonNetwork.GameVersion = GameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        private void JoinLobby()
        {
            PhotonNetwork.JoinLobby(Lobby);
        }

        private void CreateRoom()
        {
            CloseDialog();
            PhotonNetwork.CreateRoom(DialogInputField.text, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
            m_ScreenUIManager.ToggleLobbyUI(false);
        }

        private void JoinRoom()
        {
            CloseDialog();
            PhotonNetwork.JoinRoom(DialogInputField.text);
        }

        private void Handle_OnAnchorHosted(object sender, string id)
        {
            // We have a hosted anchor. Saving it in the custom properties of the room
            // so that people who join later can get the Id.
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable()
            {
                {"SharedAnchor", id}
            });

            if (OnAnchorHosted != null)
            {
                OnAnchorHosted(this, id);
            }
        }

        private void Handle_OnPlayerScored(object sender, int e)
        {
            OnPlayerScored(this, e);
        }

        private IEnumerator CheckPlaneSize()
        {
            do
            {
                // nothing
                yield return null;
            } while (HighestPlaneSize < RequiredScannedSpaceSize);
            InitializePhotonUI();
        }

        // Public Methods

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        internal void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        public void OpenDialog(bool CreateRoom)
        {
            if (CreateRoom == true)
            {
                IsDialogCreateRoom = true;
            }
            else
            {
                IsDialogCreateRoom = false;
            }
            IsDialogShown = true;
            DialogInputField.Select();
            DialogInputField.ActivateInputField();
            DialogAnimator.SetBool("IsHidden", false);
            CreateRoomButton.interactable = false;
            JoinRoomButton.interactable = false;
        }

        public void CloseDialog()
        {
            IsDialogShown = false;
            DialogAnimator.SetBool("IsHidden", true);
        }

        public void SubmitDialog()
        {
            if (IsDialogCreateRoom == true)
            {
                CreateRoom();
            }
            else
            {
                JoinRoom();
            }
        }

        // Callbacks

        public override void OnConnectedToMaster()
        {
            m_ScreenUIManager.SetSnackbarText("Connected to Master");
            CreateRoomButton.interactable = true;
            JoinRoomButton.interactable = true;
            if (PhotonNetwork.InLobby == false)
            {
                JoinLobby();
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            m_ScreenUIManager.SetSnackbarText("Disconnected from Server: " + cause.ToString());
            m_ScreenUIManager.ToggleLobbyUI(true);
            if (!PhotonNetwork.IsConnected)
            {
                CreateRoomButton.interactable = false;
                JoinRoomButton.interactable = false;
                Connect();
            }
            Debug.LogWarning("Disconnected from Server: " + cause.ToString());
        }

        public override void OnJoinedLobby()
        {
            m_ScreenUIManager.SetSnackbarText($"Connected to Lobby: {PhotonNetwork.CurrentLobby.Name}");
            m_ScreenUIManager.SetSnackbarText($"Keep Scanning the space around you, until the Area Size is at least {RequiredScannedSpaceSize}");
        }

        public override void OnJoinedRoom()
        {
            if (IsDialogShown == true)
            {
                IsDialogShown = false;
                DialogAnimator.SetBool("IsHidden", true);
            }
            m_ScreenUIManager.SetSnackbarText($"Successfully joined {PhotonNetwork.CurrentRoom.Name}");
            m_ScreenUIManager.ToggleLobbyUI(false);
            m_ScreenUIManager.SetTopbarLabelText("Player Count:");
            m_ScreenUIManager.SetTopbarCountText($"{PhotonNetwork.PlayerList.Length}");
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                ARPongMasterClient masterClient = PlayerPrefab.AddComponent<ARPongMasterClient>();
                masterClient.searchMessage = "Searching for Surfaces...";
                masterClient.AnchorPrefabName = AnchorPrefabName;
                masterClient.FirstPersonCamera = PlayerPrefab.GetComponentInChildren<Camera>();
                masterClient.m_ScreenUIManager = m_ScreenUIManager;
                masterClient.m_ARCoreWorldOriginHelper = PlayerPrefab.GetComponentInChildren<ARCoreWorldOriginHelper>();
                masterClient.m_SessionHandler = PlayerPrefab.GetComponent<ARPongSessionHandler>();
                masterClient.m_SessionHandler.m_ScreenUIManager = masterClient.m_ScreenUIManager;
                masterClient.OnAnchorHosted += Handle_OnAnchorHosted;
#if UNITY_EDITOR
                masterClient.IsTesting = true;
#elif UNITY_ANDROID
                masterClient.IsTesting = false;
#endif
            }
            else
            {
                ARPongUserClient userClient = PlayerPrefab.AddComponent<ARPongUserClient>();
                userClient.searchMessage = "Searching for Surfaces...";
                userClient.AnchorPrefabName = AnchorPrefabName;
                userClient.FirstPersonCamera = PlayerPrefab.GetComponentInChildren<Camera>();
                userClient.m_ScreenUIManager = m_ScreenUIManager;
                userClient.m_ARCoreWorldOriginHelper = PlayerPrefab.GetComponentInChildren<ARCoreWorldOriginHelper>();
                userClient.m_SessionHandler = PlayerPrefab.GetComponent<ARPongSessionHandler>();
                userClient.m_SessionHandler.client = userClient;
                userClient.m_SessionHandler.m_ScreenUIManager = userClient.m_ScreenUIManager;
            }
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            m_ScreenUIManager.SetSnackbarText($"Failed to Join Room ({returnCode}): {message}");
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            m_ScreenUIManager.SetSnackbarText($"Failed to Create Room ({returnCode}): {message}");
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            m_ScreenUIManager.SetSnackbarText($"{PhotonNetwork.PlayerList.Length}");
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            if (otherPlayer.IsMasterClient)
            {
                PhotonNetwork.Disconnect();
            }
            else
            {
                m_ScreenUIManager.SetTopbarCountText($"{PhotonNetwork.PlayerList.Length}");
            }

        }
    }
}