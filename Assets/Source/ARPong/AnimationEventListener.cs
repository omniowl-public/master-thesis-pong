﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Thesis.ARPong
{
    public class AnimationEventListener : MonoBehaviour
    {
        public event EventHandler OnEvent;

        public void CallOnEvent()
        {
            OnEvent(this, null);
        }
    }
}
