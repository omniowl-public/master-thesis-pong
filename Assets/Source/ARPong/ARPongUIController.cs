﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.ARPong
{
    public class ARPongUIController : MonoBehaviour
    {
        [Serializable]
        public struct PlayerUIObject
        {
            public Text PlayerText;
            public Text OpposingPlayerText;
        }

        public PlayerUIObject Player1;
        public PlayerUIObject Player2;
        public PlayerUIObject Spectator;
        public Text RoomName;
        private ARPongLauncher m_Launcher;

        public void Start()
        {
            RoomName.text = PhotonNetwork.CurrentRoom.Name;
            if (!PhotonNetwork.IsMasterClient)
            {
                m_Launcher.OnPlayerScored += Launcher_OnPlayerScored;
            }
        }

        private void Launcher_OnPlayerScored(object sender, int e)
        {
            AddOneToScore(e);
        }

        /// <summary>
        /// Adding one point to a player.
        /// </summary>
        /// <param name="player">The players id (zero-based)</param>
        public void AddOneToScore(int player)
        {
            int currentScore = 0;
            switch (player)
            {
                case 0:
                    currentScore = int.Parse(Player2.PlayerText.text);
                    currentScore += 1;
                    Player2.PlayerText.text = $"{currentScore}";
                    Player1.OpposingPlayerText.text = $"{currentScore}";
                    Spectator.PlayerText.text = $"{currentScore}";
                    break;
                case 1:
                    currentScore = int.Parse(Player1.PlayerText.text);
                    currentScore += 1;
                    Player1.PlayerText.text = $"{currentScore}";
                    Player2.OpposingPlayerText.text = $"{currentScore}";
                    Spectator.OpposingPlayerText.text = $"{currentScore}";
                    break;
                default:
                    break;
            }
        }

        public void ResetScores()
        {
            Player1.PlayerText.text = "0";
            Player1.OpposingPlayerText.text = "0";
            Player2.PlayerText.text = "0";
            Player2.OpposingPlayerText.text = "0";
        }
    }
}