﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Thesis.ARPong
{
    [RequireComponent(typeof(BoxCollider))]
    public class PlayZoneListener : MonoBehaviour
    {
        public ARPongGameManager manager;

        public void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag.Equals("Ball"))
            {
                manager.ResetBall();
            }
        }
    }
}
