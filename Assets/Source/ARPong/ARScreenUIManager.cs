﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.ARPong
{
    public class ARScreenUIManager : MonoBehaviour
    {
        public Text SnackbarTextObj;
        public Text TopTextLabelObj;
        public Text TopTextCountObj;

        public GameObject TopBarObj;
        public GameObject LobbyUI;

        private bool IsLobbyShown = false;

        public void SetSnackbarText(string text)
        {
            SnackbarTextObj.text = text;
        }

        public void SetTopbarLabelText(string text)
        {
            TopTextLabelObj.text = text;
        }

        public void SetTopbarCountText(string text)
        {
            TopTextCountObj.text = text;
        }

        /// <summary>
        /// Toggles the Lobby UI on or off.
        /// </summary>
        /// <param name="newState">true to enable LobbyUI and Disable Top Bar, false for the opposite.</param>
        public void ToggleLobbyUI(bool newState)
        {
            IsLobbyShown = newState;
            LobbyUI.SetActive(IsLobbyShown);
            TopBarObj.SetActive(!IsLobbyShown);
        }

        public void ExitGame()
        {
            if (IsLobbyShown == true)
            {
                ToggleLobbyUI(false);
            }
            else
            {
                PhotonNetwork.LeaveRoom();
                Application.Quit();
            }
        }
    }
}