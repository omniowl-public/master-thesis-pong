﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Thesis.ARPong
{
    [RequireComponent(typeof(Rigidbody))]
    public class ARPongBall : MonoBehaviour
    {
        public float SpeedUpFactor;
        private Rigidbody rBody;
        private float Speed = 1.0f;

        private void Start()
        {
            rBody = GetComponent<Rigidbody>();
        }

        public void OnCollisionEnter(Collision collision)
        {
            Vector3 normal = collision.GetContact(0).normal.normalized;
            Vector3 direction = rBody.velocity;

            rBody.velocity = Vector3.Reflect(direction, normal).normalized * Speed;

            if (collision.gameObject.tag.Equals("Player"))
            {
                Speed += SpeedUpFactor;
            }
        }
    }

}