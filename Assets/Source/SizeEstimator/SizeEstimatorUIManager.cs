﻿using GoogleARCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Thesis.SizeEstimator
{
    public class SizeEstimatorUIManager : MonoBehaviour
    {
        public Text SnackbarText;
        public Text SizeText;

        private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();
        private float BiggestSize;

        // Update is called once per frame
        void Update()
        {
            Session.GetTrackables(m_AllPlanes);
            foreach (DetectedPlane plane in m_AllPlanes)
            {
                if (plane.TrackingState == TrackingState.Tracking)
                {
                    float size = plane.ExtentX * plane.ExtentZ;
                    BiggestSize = (size > BiggestSize ? size : BiggestSize);
                    SizeText.text = $"{BiggestSize.ToString("#.##")}";
                }
            }
        }
    }
}